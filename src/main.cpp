/*
 *
 * Copyright (C) Patryk Jaworski (blog.regalis.tech)
 *
 * Author: Patryk Jaworski <regalis@regalis.tech>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <filesystem>
#include <iostream>
#include <string_view>
#include <type_traits>

#include <boost/crc.hpp>
#include <boost/iostreams/device/mapped_file.hpp>
#include <fmt/printf.h>
#include <fmt/ranges.h>
#include <range/v3/all.hpp>
#include <ranges>

#include "uf2.hpp"

constexpr uint32_t calculate_rp_checksum(
  const std::ranges::viewable_range auto data)
{
    constexpr auto bits_no = 32;
    constexpr auto polynomial = 0x04c11db7;
    constexpr auto initial_value = 0xffffffff;
    constexpr auto final_xor = 0;
    constexpr auto input_reflection = false;
    constexpr auto output_reflection = false;

    auto result = boost::crc<bits_no,
                             polynomial,
                             initial_value,
                             final_xor,
                             input_reflection,
                             output_reflection>(data.data(), data.size());

    if constexpr (std::endian::native != std::endian::little) {
        return std::byteswap(result);
    }
    return result;
}

std::array<uint8_t, 4> to_bytes_array(uint32_t data)
{
    auto bytes = std::array<uint8_t, 4>{};
    auto crc_span = std::span<uint8_t>{reinterpret_cast<uint8_t*>(&data), 4};
    ranges::copy(crc_span, bytes.begin());
    return bytes;
}

int main(int argc, char** argv)
{
    std::filesystem::path input_file;

    if (argc < 2) {
        fmt::print(stderr, "Usage: regalis-pico-bin2uf2 INPUT_FILE \n");
        return 1;
    }

    input_file = argv[1];

    boost::iostreams::mapped_file_source mapped_file(input_file);
    if (!mapped_file.is_open()) {
        fmt::print(
          stderr, "Unable to open an input file '{}'\n", input_file.c_str());
        return 2;
    }

    const auto input_data = std::span{mapped_file.data(), mapped_file.size()};

    const auto bootloader_code = input_data | std::views::take(252);
    const auto bootloader_checksum =
      to_bytes_array(calculate_rp_checksum(bootloader_code));
    const auto rest = input_data | std::views::drop(256);

    const auto firmware =
      ranges::views::concat(bootloader_code, bootloader_checksum, rest);

    fmt::print(stderr,
               "Size of the firmware : {} bytes\n"
               "Bootloader's checksum: {::#x}\n",
               firmware.size(),
               bootloader_checksum);

    // ranges::copy(firmware, ranges::ostream_iterator<uint8_t>(std::cout));
    std::vector<uf2_block> firmware_uf2 = to_uf2(firmware);

    for (const auto& block : firmware_uf2) {
        ranges::copy(as_bytes(block),
                     ranges::ostream_iterator<uint8_t>(std::cout));
    }
}
