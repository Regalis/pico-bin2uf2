/*
 *
 * Copyright (C) Patryk Jaworski (blog.regalis.tech)
 *
 * Author: Patryk Jaworski <regalis@regalis.tech>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef UF2_HPP
#define UF2_HPP

#include <concepts>
#include <cstdint>
#include <fmt/printf.h>
#include <fmt/ranges.h>
#include <range/v3/view/interface.hpp>
#include <ranges>
#include <span>
#include <type_traits>

constexpr uint32_t RP2040_XIP_START_ADDR = 0x10000000;

struct uf2_block
{
    // 32 byte header
    uint32_t magic_start0 = 0x0A324655; // ASCII "UF2\n"
    uint32_t magic_start1 = 0x9E5D5157;
    uint32_t flags =
      0x00002000; // family id holds a value identifying the board family
    uint32_t target_addr = RP2040_XIP_START_ADDR; // RP2040's XIP (Flash)
    uint32_t payload_size = 0;
    uint32_t block_number = 0;
    uint32_t number_of_blocks = 0;
    uint32_t family_id = 0xe48bff56; // Raspberry Pi RP2040
    uint8_t data[476] = {
      0,
    };
    uint32_t magic_end = 0x0AB16F30;
};

constexpr std::array<uint8_t, 512> as_bytes(const uf2_block& block)
{
    std::array<uint8_t, 512> output;
    uf2_block buffer = block;
    if constexpr (std::endian::native == std::endian::big) {
        buffer.magic_start0 = std::byteswap(buffer.magic_start0);
        buffer.magic_start1 = std::byteswap(buffer.magic_start1);
        buffer.flags = std::byteswap(buffer.flags);
        buffer.target_addr = std::byteswap(buffer.target_addr);
        buffer.payload_size = std::byteswap(buffer.payload_size);
        buffer.block_number = std::byteswap(buffer.block_number);
        buffer.number_of_blocks = std::byteswap(buffer.number_of_blocks);
        buffer.family_id = std::byteswap(buffer.family_id);
        buffer.magic_end = std::byteswap(buffer.magic_end);
    }
    auto bytes_view =
      std::span<uint8_t>{reinterpret_cast<uint8_t*>(&buffer), 512};
    std::ranges::copy(bytes_view, output.begin());
    return output;
}

std::vector<uf2_block> to_uf2(
  const std::ranges::sized_range auto firmware_range,
  std::size_t chunk_size = 256)
{

    std::vector<uf2_block> blocks;
    const auto chunked_firmware =
      firmware_range | std::views::chunk(chunk_size);

    const auto number_of_blocks =
      (firmware_range.size() + chunk_size - 1) / chunk_size;

    for (const auto [index, chunk] : std::views::enumerate(chunked_firmware)) {

        uf2_block block;
        block.target_addr = RP2040_XIP_START_ADDR + (index * chunk_size);
        block.payload_size = chunk_size;
        block.block_number = index;
        block.number_of_blocks = number_of_blocks;
        std::ranges::copy(chunk, block.data);

        blocks.push_back(block);
    }
    return blocks;
}

#endif
